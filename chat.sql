-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2012 at 06:40 PM
-- Server version: 5.1.54
-- PHP Version: 5.3.5-1ubuntu7.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chat`
--

-- --------------------------------------------------------

--
-- Table structure for table `dash`
--

CREATE TABLE IF NOT EXISTS `dash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(512) NOT NULL,
  `to` varchar(512) DEFAULT NULL,
  `message` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `dash`
--

INSERT INTO `dash` (`id`, `username`, `to`, `message`) VALUES
(1, 'saman', '', 'asd asdasdasd asdasd dsadasdasd'),
(2, 'gayan', '', 'asd asdasdasd asdasd dsadasdasd'),
(3, 'danuka', '', 'asd asdasdasd asdasd dsadasdasd'),
(4, 'piyak', '', 'asd asdasdasd asdasd dsadasdasd'),
(5, 'siril', '', 'asd asdasdasd asdasd dsadasdasd'),
(6, 'kamal', '', 'asd asdasdasd asdasd dsadasdasd'),
(7, 'username', NULL, 'bz asd asd asd asd'),
(8, 'gayanhewa', NULL, 'bz asd asd asd asd'),
(9, 'gayanhewa', NULL, 'bz asd asd asd asd'),
(10, 'aaa', 'bbb', 'xxx');

-- --------------------------------------------------------

--
-- Table structure for table `unfollow`
--

CREATE TABLE IF NOT EXISTS `unfollow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `unfollow` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `unfollow`
--

INSERT INTO `unfollow` (`id`, `user`, `unfollow`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(256) NOT NULL,
  `mobile_phone` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `mobile_phone`) VALUES
(1, 'saman', 'a5e76305b198a4b7581970ddd562ca2f'),
(2, 'gayan', '9c10f3906393b93aa17cea298fea3df6'),
(3, 'piyak', '6659fcf5118dbedc1b5323cec1a14352'),
(4, 'kamal', '28389894aac26f246d2c5db56c87203d'),
(5, 'gayanhewa', 'fb57bc1434fa37ba3c79e71ee2184c49'),
(6, 'asdasdasd', 'sdadq2e1asd');
